(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 11.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      7890,        188]
NotebookOptionsPosition[      7382,        171]
NotebookOutlinePosition[      7716,        186]
CellTagsIndexPosition[      7673,        183]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"M", " ", "=", " ", "100"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"x1", " ", "=", " ", "0"}], ";", " ", 
  RowBox[{"x2", "=", "1"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"sol", "=", 
   RowBox[{"RecurrenceTable", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       RowBox[{
        RowBox[{"x", "[", 
         RowBox[{"n", "+", "1"}], "]"}], "\[Equal]", 
        RowBox[{
         RowBox[{
          RowBox[{"(", 
           RowBox[{"1", "+", 
            RowBox[{"1", "/", "n"}]}], ")"}], "*", 
          RowBox[{"x", "[", "n", "]"}]}], "-", 
         RowBox[{"x", "[", 
          RowBox[{"n", "-", "1"}], "]"}]}]}], ",", 
       RowBox[{
        RowBox[{"x", "[", "1", "]"}], "\[Equal]", "x1"}], ",", 
       RowBox[{
        RowBox[{"x", "[", "2", "]"}], "\[Equal]", "x2"}]}], "}"}], ",", "x", 
     ",", 
     RowBox[{"{", 
      RowBox[{"n", ",", "1", ",", " ", "M"}], "}"}]}], "]"}]}], 
  ";"}]}], "Input",
 CellChangeTimes->{{3.787379187397728*^9, 3.7873792072658463`*^9}, {
  3.7873792519099483`*^9, 3.787379293695889*^9}, {3.7873793344030533`*^9, 
  3.7873793348344088`*^9}, {3.7948456467387533`*^9, 3.794845657872081*^9}, {
  3.794845725222464*^9, 3.794845779676939*^9}, {3.794845909591267*^9, 
  3.794845937805233*^9}, {3.7948459681956882`*^9, 3.794846061403916*^9}, {
  3.794846194072467*^9, 3.794846210703938*^9}, {3.794846241406335*^9, 
  3.7948462416139717`*^9}},
 CellLabel->"In[55]:=",ExpressionUUID->"2750638c-587b-469f-8a89-01b94e5692d0"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"ListLinePlot", "[", 
  RowBox[{"sol", ",", " ", 
   RowBox[{"PlotMarkers", "\[Rule]", "Automatic"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.787379270363132*^9, 3.78737927152427*^9}, {
  3.79484578884459*^9, 3.794845816679468*^9}, {3.794845870415783*^9, 
  3.79484588246222*^9}, {3.794845963554017*^9, 3.794845963973584*^9}, {
  3.7948461562263517`*^9, 3.794846170072113*^9}},
 CellLabel->"In[58]:=",ExpressionUUID->"37aa6972-ce50-48ad-8f13-27905149577c"],

Cell[BoxData[
 GraphicsBox[{{}, {{{}, {}, 
     {RGBColor[0.368417, 0.506779, 0.709798], PointSize[
       NCache[
        Rational[1, 72], 0.013888888888888888`]], AbsoluteThickness[1.6], 
      LineBox[CompressedData["
1:eJxdlHtMU2cYxqsEh6AbKDomQyhDWBxWVtkc4/YUCpRy7Y0Wh0QR4rJwmTAu
Y7ejLJEgzilmMi4quIiTkMAY0LlsLSzDMMdNpAEZgQFFRLC0QFFcxub6nj/c
lzQnvz55nvc933fej5uSJU1bz+FwUv/9PX1a1kIw59mFZ/+3IX5EbP8/3ZG4
T2N5OqH796drifhlCP9bLLtBxtNLefoOYne0b6zSbqwyUp4HhoPKeefnl4k9
4XZy1afc7i7xqyhJ0bVGXp0n/24MtJsex5wzE3vDsUVfIl6ZIObhBZeDxhO3
Z8jvg1he3Eyb2kz8OpS7ov6wyrtHzAdS9JluFVPk3weNo/+msgo23xdhOw5X
u/81S/wGXNZVJ1aOjZP/Tbx09QcrtZzN34/wtm2S4v554rcwqi09sMFphPx+
4Be59lmvLBO/jZZ07qrBYYHYH7mrAibDeZD8AWg7yS8cKGH3JxA9ws+S523Y
/QtC4ZO8PXuSeskfjAO6s0ent7H5wFptp5kZMlqYAVxNdSK73F/JL8DKtSzz
QuWShRkBDk3FRn5cbCI9BFcKrfNSflaTPwSCxkn1YVf2fEORlXDhTMvORdJD
0etk2y1ZuUR+IS73HPG6W7NI+UL0GJPltZeIOWGIKIo/1rf1IulhGBq61Z7r
QXmccBhds4d+fJ7qMeGwcRPlfGFoIT0CHyb1RZvrqF8mAubmgoKOTHofjgjc
DXYGdWM76SLwamp9x71N5I8Evy7fb66dzY9EWJM4o2L1JulijPRXB59upv1m
xBC59N6wtmXPIwqagfcmNOPdpEehyWPM0y+A9psTjXy7pedKRHQeTDQ26StN
x1r7SY9B3dFfAmxv0nwxMSh+kOrx/adsfixk13WvqWV3SI/F43d25r8oZ7+X
OMjUTj6lDWx+HFLHwwwjt3Skx+PaT9r04U6DxY94vKuySzMOUj4Tj7KV83cc
3IctrI1HbE9zg/PsQ/JLEDJU0FXziPIhgeTvy1zvRPqeGQlkqhMDRoeH5Jeg
cdHKB9vZeZDC86O0G9/mjFJ9KYyqjOtxQTTPjBSZ9WdNqr00b1opvvOyTuzI
GaP6MozsTt78StYc+WWQCMStaULKZ2Q4pV2LUzfQPGplsPdbqVd884D8ckTo
nLP7FJQPOaDZ4R/41Z9UX46WzcVJPaOz5JdDcihgYvkI278CZVXvrwv4YILq
K7B+e6JTtjPdB4wCDmufF3tlsf0rEKwTdaaLJql+ArZcFKs9Dt4nfwKaPH+T
NOSz/Seg/vT9/Xlbp6h+Apq53ce7amfIr8SuwHpT5yds/0os88vrDYN0XzFK
7Ft6ciZ0ju4zrRJ8X65tw3G2fxW+zp6cTflST/VVMAgucPP87pFfhb1X5h3P
FbH9q7Clq1QTEzpN9RNhr0y9LSyZDv4HWV7x0A==
       "]]}}, {
     {RGBColor[0.368417, 0.506779, 0.709798], AbsolutePointSize[6], 
      AbsoluteThickness[1.6], GeometricTransformationBox[InsetBox[
        StyleBox["\<\"\[FilledCircle]\"\>",
         StripOnInput->False,
         FontSize->8.96], {0., 0.}], CompressedData["
1:eJxdlHtMU2cYxosEh6AbKDomQyhDWBxWVtkc4/YUCpRy7Y0Wh0QR4rJwmTAu
Y7ejLpEgzilmMi4quIiTkMAY0LlsLSzDMMdNpAEZgQFFRLC0QKu4jLnxnj/c
l5w0T5/8nvc933fej5uaLU235nA4aU8fq6fPOg67FkI4zy48+78t6UekHf7n
O5Hu06z9OqP793/XEumXIfxvsdodMp5eytN3kPZA+4Yq7YYqI+V5Yji4nHd+
fpm0F9xPrviW298l/SpKUnWtUVfnid+FgXbT49hzZtI+cGrRl4gtE6R5eMH1
gPH47RnifRHHi59pU5tJvw7lzug/rPPvkeYDqfos94op4vdC4xSwsayCzfdD
+PZD1R5/zZJ+A65W1UmVY+PEv4mXrv5grZaz+fsQ0bZVUtw/T/otjGpL9693
HiHeH/wTbn02lmXSb6Mlg7ticFwgHYC8FQGT6TJIfCDaTvKLBkrY/QlCj/Cz
lHlbdv+CUfQkf/fu5F7iQ7Bfd/bI9FY2H1it7TQzQ8Y1zQBupjqRfd6vxAtg
uZZtXqhcWtOMAAen4qI+LjaRH4orRTb5qT+riQ+FoHFSfciNPd8wZCdeONOy
Y5H8MPQ623VLLJeIF+Jyz2HvuzWLlC9EjzFFXnuJNCcckScSjvZtuUh+OIaG
brXneVIeJwJGt5yhH5+nekwEbN1FuV8YWsiPxIfJfTHmOuqXiYS5ubCwI4ve
hyMCd729Qd3YTr4IvJpav3EfE/FR4NcV+M+1s/lRCG8SZ1as3CRfjJH+6pDT
zbTfjBgi194bNnbseURDM/DehGa8m/xoNHmOefkH0n5zYlBgv/RciYjOg4nB
Rn2l6WhrP/mxqDvyS6DdTZovJhbFD9I8v/+UzY+D7LruNbXsDvlxePzOjoIX
5ez3Eg+Z2tm3tIHNj0faeLhh5JaO/ARc+0mbMdxpWOORgHdV9unGQcpnElBm
OX/H0WN4TWsTENfT3OAy+5B4CUKHCrtqHlE+JJD8fZnrk0TfMyOBTHV8wOj4
kHgJGhetfbGNnQcpvD5Kv/Ft7ijVl8KoyrweH0zzzEiRVX/WpNpD86aV4jtv
m6SO3DGqL8PIrpRNr2TPES+DRCBuTRdSPiPDKe1qvLqB5lErg4O/pV7xzQPi
5YjUueT0KSgfckCzPSDoqz+pvhwtm4qTe0ZniZdDcjBwYvkw278CZVXvWwV+
MEH1FVi3Lck5x4XuA0YBx9XPi72z2f4VCNGJOjNEk1Q/EZsvitWeB+4Tn4gm
r98kDQVs/4moP31/X/6WKaqfiGZu97Gu2hnildgZVG/q/ITtX4llfnm9YZDu
K0aJvUtPzoTN0X2mVYLvx7VrOMb2r8LXOZOzqV/qqb4KBsEFbr7/PeJV2HNl
3uncCbZ/FTZ3lWpiw6apfhIclGm3hSXTIf8Abjbx0g==
       
       "]]}, {}}}, {}, {}, {}, {}},
  AspectRatio->NCache[GoldenRatio^(-1), 0.6180339887498948],
  Axes->{True, True},
  AxesLabel->{None, None},
  AxesOrigin->{0., 0},
  DisplayFunction->Identity,
  Frame->{{False, False}, {False, False}},
  FrameLabel->{{None, None}, {None, None}},
  FrameTicks->{{Automatic, Automatic}, {Automatic, Automatic}},
  GridLines->{None, None},
  GridLinesStyle->Directive[
    GrayLevel[0.5, 0.4]],
  ImagePadding->All,
  Method->{"CoordinatesToolOptions" -> {"DisplayFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& ), "CopiedValueFunction" -> ({
        (Identity[#]& )[
         Part[#, 1]], 
        (Identity[#]& )[
         Part[#, 2]]}& )}},
  PlotRange->{{0., 100.}, {-1.4127378919377376`, 1.5}},
  PlotRangeClipping->True,
  PlotRangePadding->{{
     Scaled[0.02], 
     Scaled[0.02]}, {
     Scaled[0.05], 
     Scaled[0.05]}},
  Ticks->{Automatic, Automatic}]], "Output",
 CellChangeTimes->{{3.787379278781087*^9, 3.7873792986450853`*^9}, 
   3.787379340986446*^9, 3.787379372580941*^9, 3.794845789528854*^9, 
   3.794845819898491*^9, {3.79484587240059*^9, 3.794845883055492*^9}, {
   3.794845939437353*^9, 3.794846063444991*^9}, {3.7948461614833097`*^9, 
   3.794846245283526*^9}},
 CellLabel->"Out[58]=",ExpressionUUID->"00ea19cc-23ee-49a4-a3e1-cb9d8741e302"]
}, Open  ]]
},
WindowSize->{808, 728},
WindowMargins->{{Automatic, 54}, {41, Automatic}},
FrontEndVersion->"11.3 for Linux x86 (64-bit) (March 6, 2018)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 1528, 38, 101, "Input",ExpressionUUID->"2750638c-587b-469f-8a89-01b94e5692d0"],
Cell[CellGroupData[{
Cell[2111, 62, 480, 8, 31, "Input",ExpressionUUID->"37aa6972-ce50-48ad-8f13-27905149577c"],
Cell[2594, 72, 4772, 96, 263, "Output",ExpressionUUID->"00ea19cc-23ee-49a4-a3e1-cb9d8741e302"]
}, Open  ]]
}
]
*)

